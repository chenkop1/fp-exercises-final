## Ramda: [https://ramdajs.com/docs/](https://ramdajs.com/docs/)

## Git url: [https://tinyurl.com/fp-final](https://tinyurl.com/fp-final)

# Functional Programming Principles:

-
  ## Pure Functions:
  A function is pure **if and only if** it meets both of the following conditions:
  - **Deterministic**: Same arguments -> Same result
  - **No Side Effects**

  <br/>
  
  ### **Deterministic Example:**
  ❌ **Bad Example (Not a Pure Function):**
  ```js
  let x = 5;
  const func = (a, b) => a + b + x;

  console.log(func(2, 3)); // 10
  x++;
  console.log(func(2, 3)); // 11
  ```
  ✅ **Good Example (Pure Function):**
  ```js
  const func = (a, b, x) => a + b + x;

  console.log(func(2, 3, 5)); // 10
  ```

  <br/>
  
  ### **No Side Effects Example:**
  ❌ **Bad Example (Not a Pure Function):**
  ```js
  let x = 5;
  const func = (a, b) => {
    x++;
    return a + b;
  };
  
  console.log(x); // 5
  func(2, 3);
  console.log(x); // 6
  ```
  ✅ **Good Example (Pure Function):**
  ```js
  let x = 5;
  const func = (a, b) => a + b;

  console.log(x); // 5
  func(2, 3);
  console.log(x); // 5
  ```

  <br/>
*
  ## Immutability (const only! no let!)

  
  ### **Immutability Example:**
  ❌ **Bad Example (Not Immutable):**
  ```js
  let arr = [1, 2, 3];
  const addElement = elem => arr.push(elem);

  console.log(arr); // [1, 2, 3]
  addElement(2);
  console.log(arr); // [1, 2, 3, 2]
  ```
  ✅ **Good Example (Immutable):**
  ```js
  const arr = [1, 2, 3];
  const addElement = (arr, elem) => [...arr, elem];

  console.log(arr);    // [1, 2, 3]
  const newArr = addElement(arr, 2);
  console.log(arr);    // [1, 2, 3]
  console.log(newArr); // [1, 2, 3, 2]
  ```

  ✅ **Good Example (Immutable Object Update):**
  ```js
  const person = { name: 'dan', age: 13 };
  const withAge = (person, age) => ({ ...person, age: age });

  console.log(withAge(person, 15)); // {name: 'dan', age: 15}
  console.log(person);              // {name: 'dan', age: 13}
  ```

  <br/>

-
  ## Functions As First-Class Entities:
  A function can be treated as a first-class entity **if and only if** it meets one of the following conditions:
  - **Refer to it from constants and variables**
  - **Pass it as a parameter to other functions**
  
  
  ### **Refer to it from constants and variables Example:**
  ```js
  const doubleSum = (a, b) => (a + b) * 2;
  
  doubleSum(2, 3);
  ```
  
  ### **Pass it as a parameter to other functions Example:**
  ```js
  const sum = (a, b) => a + b;
  const subtraction = (a, b) => a - b;
  const doubleOperator = (f, a, b) => f(a, b) * 2;
  
  doubleOperator(sum, 3, 1);         // 8
  doubleOperator(subtraction, 3, 1); // 4
  ```
  <br/>

*
  ## High-Order Functions:
  A function is considered **high-order** **if and only if** it meets one of the following conditions:
  - **Takes one or more functions as parameters**
  - **Returns a function as its result**
  
  
  ### **Takes one or more functions as parameters Example:**
  ```js
  const sum = (a, b) => a + b;
  const subtraction = (a, b) => a - b;
  const doubleOperator = (f, a, b) => f(a, b) * 2;
  
  doubleOperator(sum, 3, 1);         // 8
  doubleOperator(subtraction, 3, 1); // 4
  ```
  
  ### **Returns a function as its result Example:**
  ```js
  const add = a => b => a + b;
  
  const addTwo = add(2);
  // add(2) = b => 2 + b;
  addTwo(4); // 6
  ```
  
  ### **Filter Example:**
  ```js
  const arr = [5, 4, 2, 1, 2, 3, 4];
  const isEven = num => num % 2 == 0;
  
  const evens = arr.filter(isEven);
  
  // evens = [4, 2, 2, 4]
  ```
  
  ### **Map Example:**
  ```js
  const arr = [5, 4, 2, 1, 2, 3, 4];
  const double = num => num * 2;
  
  const doubledArr = arr.map(double);
  
  // doubledArr = [10, 8, 4, 2, 4, 6, 8]
  ```
  
  ### **Reduce Example:**
  ```js
  const arr = [2, 4, 5, 3, 7];
  const add = (acc, val) => acc + val;
  
  console.log(arr.reduce(add, 0)); // 21
  console.log(arr.reduce(add));    // 21
  ```
  <br/>

-
  ## Declarative Programming (WHAT instead of HOW)
  
  ### **Declarative vs Imperative Example:**
  ```js
  // Imperative (How)
  const arr = [5, 4, 2, 1, 2, 3, 4];
  const doubledArr = [];
  for (let i = 0; i < arr.length; i++) {
    doubledArr.push(arr[i] * 2);
  }
  
  // Declarative (What)
  const arr = [5, 4, 2, 1, 2, 3, 4];
  const double = num => num * 2;
  const doubledArr = arr.map(double);
  ```
  