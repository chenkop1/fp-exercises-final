/*
You are given strings str and col.
String str represents a table in CSV (comma-separated-values) format,
where rows are separated by new line characters ('\n') and each row consists of one or more fields separated by commas (',').

The first row contains the names of the columns. 
The following rows contain values.

For example, the table below is presented by the following string:
str = "id,name,age,act.,room,dep.\n1,Jack,68,T,13,8\n17,Betty,28,F,15,7".

+----+-------+-----+------+------+------+
| id |  name | age | act. | room | dep. |
+----+-------+-----+------+------+------+
|  1 |  Jack |  68 |   T  |  13  |   8  |
| 17 | Betty |  28 |   F  |  15  |   7  |
+----+-------+-----+------+------+------+

String col is the name of a column in the table described by str that contains only integers.
Your task is to find the maximum value in that column. 
In the example above, for col = "age", the maximum value is 68.
In the example above, for col = "room", the maximum value is 15.
In the example above, for col = "dep.", the maximum value is 8.

Write a function:
const maxValue = (str, col) => {...};

Which, given two string str and col consisting of N and M characters, respectively,
returns the maximum value in column col of the table described by str.
*/

import { split, zipObj } from "ramda";

/**
 * @param {string} str 
 * @param {string} col 
 * @returns {number} max value in the given column
 */
// hint: read about `split`  in ramda's documentation (https://ramdajs.com/docs/#split)
// hint: read about `zipObj` in ramda's documentation (https://ramdajs.com/docs/#zipObj)
export const maxValue = (str, col) =>;