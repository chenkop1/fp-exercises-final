import { boostSingleScores, removeOverScores } from '../ex1';

const scores = [-10, 6, 20, 50, 100, -3, 75, 170, 80, 96, 0, 100, 89, 9, 219];

describe("ex1", () => {
    test("removeOverScores", () => {
        expect(removeOverScores(scores)).toEqual([-10, 6, 20, 50, 100, -3, 75, 80, 96, 0, 100, 89, 9]);
        expect(removeOverScores([])).toEqual([]);
        expect(removeOverScores([20, 100, -3])).toEqual([20, 100, -3]);
    });
    test("boostSingleScores", () => {
        expect(boostSingleScores(scores)).toEqual([-100, 60, 20, 50, 100, -30, 75, 170, 80, 96, 0, 100, 89, 90, 219]);
        expect(boostSingleScores([])).toEqual([]);
    });
});