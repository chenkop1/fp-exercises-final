import { addPointsToScoreByName, getByName, getHighestScore } from '../ex2';

const users = [
    { name: 'James', score: 30, tries: 1 },
    { name: 'Mary', score: 110, tries: 4 },
    { name: 'Mary', score: 330, tries: 7 },
    { name: 'Mary', score: 500, tries: 10 },
    { name: 'Mary', score: 700, tries: 6 },
    { name: 'Henry', score: 80, tries: 3 },
    { name: 'Mary', score: 220, tries: 5 }
];

describe("ex2", () => {
    test("getByName", () => {
        const maryUsers = [
            { name: 'Mary', score: 110, tries: 4 },
            { name: 'Mary', score: 330, tries: 7 },
            { name: 'Mary', score: 500, tries: 10 },
            { name: 'Mary', score: 700, tries: 6 },
            { name: 'Mary', score: 220, tries: 5 }
        ];
        expect(getByName('Mary', users)).toEqual(maryUsers);
    });

    test("addPointsToScoreByName", () => {
        const points = 13;
        const updatedUsers = [
            { name: 'James', score: 30, tries: 1 },
            { name: 'Mary', score: 110 + points, tries: 4 },
            { name: 'Mary', score: 330 + points, tries: 7 },
            { name: 'Mary', score: 500 + points, tries: 10 },
            { name: 'Mary', score: 700 + points, tries: 6 },
            { name: 'Henry', score: 80, tries: 3 },
            { name: 'Mary', score: 220 + points, tries: 5 }
        ];
        expect(addPointsToScoreByName(points, 'Mary', users)).toEqual(updatedUsers);
    });

    test("getHighestScore", () => {
        expect(getHighestScore(users)).toEqual(700);
    });
});
