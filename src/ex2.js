import { prop } from 'ramda';

//#region definitions

/**
 * @typedef {object} User
 * @property {string} name
 * @property {number} score
 * @property {number} tries
 */

/**
 * @description Example of users array
 * @type {User[]}
 */
const USERS = [
    { name: 'James', score: 30, tries: 1 },
    { name: 'Mary', score: 110, tries: 4 },
    { name: 'Henry', score: 80, tries: 3 },
    { name: 'Mary', score: 220, tries: 5 }
];

//#endregion definitions


//#region getByName

/**
 * @param {string} name 
 * @param {User[]} users 
 * @returns {User[]} all users with the given name
 */
// hint: read about `propEq` in ramda's documentation (https://ramdajs.com/docs/#propEq)
export const getByName = (name, users) =>;

// console.log(getByName('Mary', USERS));

//#endregion getByName


//#region addPoints

/**
 * @param {number} points
 * @param {string} name
 * @param {User[]} users
 * @returns {User[]} users array which all users with the given name get an increased score by the given points
 */
export const addPointsToScoreByName = (points, name, users) =>;

// console.log(addPointsToScoreByName(5, 'Mary', USERS));

//#endregion addPoints


//#region getHighestScore

/**
 * @param {User[]} users 
 * @returns {number} highest score 
 */
export const getHighestScore = users =>;

// console.log(getHighestScore(USERS));

//#endregion getHighestScore